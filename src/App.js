import React from 'react';
import './App.css'
import Header from './components/header.js'
import Footeer from './components/footer.js'

function App() {
  return (
    <div>
      <Header />
      <Footeer />
    </div>
  );
}

export default App;
