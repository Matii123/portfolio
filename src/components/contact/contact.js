import emailjs from "emailjs-com";
import React from 'react';

import { Container, Col, Row } from 'react-bootstrap';

export default function Contact() {
    function sendEmail(e) {
        e.preventDefault();

    emailjs.sendForm('service_eoxxl15', 'template_orwnz2l', e.target, 'aqfXu0RcRCqHtwHxT')
        .then((result) => {
            console.log(result.text);
        }, (error) => {
            console.log(error.text);
        });
        e.target.reset()
    }
    return(
        <div>
            <Container fluid className="mt-4">
                <h1 className="text-center text-white">
                  Kontakt
                </h1>
                <Row className="row pt-4 mx-auto">
                    <Col></Col>
                    <Col sm={4}>
                        <form onSubmit={sendEmail}>
                            <div className="form-group pt-2 mx-auto">
                                <input type="text" className="form-control" placeholder="Temat" name="subject"/>
                            </div>
                            <div className="form-group pt-2 mx-auto">
                                <input type="email" className="form-control" placeholder="Adres email" name="email"/>
                            </div>
                            <div className="form-group pt-2 mx-auto">
                                <textarea className="form-control" id="" cols="30" rows="8" placeholder="Twoja wiadomość" name="message"></textarea>
                            </div>
                            <div className="pt-3 mx-auto">
                                <input type="submit" className="btn btn-info" value="Wyślij wiadomość"></input>
                            </div>
                        </form>
                    </Col>
                    <Col></Col>
                </Row>
            </Container>
        </div>
    )
}