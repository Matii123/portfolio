import React, { Component } from 'react'
import { Container, Col, Row } from 'react-bootstrap';

import Card from "react-bootstrap/Card";

import portal_studencki from '../../picture/projects/portal_studencki.jpg';
import portfolio from '../../picture/projects/portfolio.jpg';
import metody_inżynierii_wiedzy from '../../picture/projects/metody_inżynierii_wiedzy.jpg';
import tetris from '../../picture/projects/tetris.jpg';
import przychodnia from '../../picture/projects/przychodnia.jpg';
import kino from '../../picture/projects/kino.jpg';

import { 
    portal_studencki_project, 
    portfolio_project, 
    metody_inżynierii_wiedzy_project,
    tetris_project,
    przychodnia_project,
    kino_project,
} from '../../date';

import '../../css/projects.css';

export default class Projects extends Component {
    render() {
        return (
            <div>
                <Container fluid className="mt-4">
                    <h1 className="text-center text-white">
                        Moje projekty
                    </h1>
                    <Row style={{ justifyContent: "center", paddingBottom: "10px" }}>
                    <Col md={4} className="project-card">
                        <Card className="project-card-view">
                        <Card.Img variant="top" src={portal_studencki} alt="card-img" />
                        <Card.Body>
                            <Card.Title>{portal_studencki_project.title}</Card.Title>
                            <Card.Text style={{ textAlign: "justify" }}>
                            {portal_studencki_project.description}
                            </Card.Text>
                                <a href={portal_studencki_project.url}><input type="button" className="btn btn-info" value="Link do kodu"></input></a>
                        </Card.Body>
                        </Card>
                    </Col>
                    <Col md={4} className="project-card">
                        <Card className="project-card-view">
                        <Card.Img variant="top" src={kino} alt="card-img" />
                        <Card.Body>
                            <Card.Title>{kino_project.title}</Card.Title>
                            <Card.Text style={{ textAlign: "justify" }}>
                            {kino_project.description}
                            </Card.Text>
                                <a href={kino_project.url}><input type="button" className="btn btn-info" value="Link do kodu"></input></a>
                        </Card.Body>
                        </Card>
                    </Col>
                    <Col md={4} className="project-card">
                        <Card className="project-card-view">
                        <Card.Img variant="top" src={przychodnia} alt="card-img" />
                        <Card.Body>
                            <Card.Title>{przychodnia_project.title}</Card.Title>
                            <Card.Text style={{ textAlign: "justify" }}>
                            {przychodnia_project.description}
                            </Card.Text>
                        </Card.Body>
                        </Card>
                    </Col>
                    <Col md={4} className="project-card">
                        <Card className="project-card-view">
                        <Card.Img variant="top" src={metody_inżynierii_wiedzy} alt="card-img" />
                        <Card.Body>
                            <Card.Title>{metody_inżynierii_wiedzy_project.title}</Card.Title>
                            <Card.Text style={{ textAlign: "justify" }}>
                            {metody_inżynierii_wiedzy_project.description}
                            </Card.Text>
                                <a href={metody_inżynierii_wiedzy_project.url}><input type="button" className="btn btn-info" value="Link do kodu"></input></a>
                        </Card.Body>
                        </Card>
                    </Col>
                    <Col md={4} className="project-card">
                        <Card className="project-card-view">
                        <Card.Img variant="top" src={portfolio} alt="card-img" />
                        <Card.Body>
                            <Card.Title>{portfolio_project.title}</Card.Title>
                            <Card.Text style={{ textAlign: "justify" }}>
                            {portfolio_project.description}
                            </Card.Text>
                                <a href={portfolio_project.url}><input type="button" className="btn btn-info" value="Link do kodu"></input></a>
                        </Card.Body>
                        </Card>
                    </Col>
                    <Col md={4} className="project-card">
                        <Card className="project-card-view">
                        <Card.Img variant="top" src={tetris} alt="card-img" />
                        <Card.Body>
                            <Card.Title>{tetris_project.title}</Card.Title>
                            <Card.Text style={{ textAlign: "justify" }}>
                            {tetris_project.description}
                            </Card.Text>
                        </Card.Body>
                        </Card>
                    </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}