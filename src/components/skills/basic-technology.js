import React from "react";
import { 
    Col, 
    Row, 
} from "react-bootstrap";
import { 
    SiCloudera,
    SiCsharp,
    SiJavascript,
} from "react-icons/si";
import{
    DiPhp,
} from "react-icons/di";
import{
    FaLaravel,
} from "react-icons/fa";
import {
  DiDjango,
  DiWordpress,
  DiReact,
} from "react-icons/di";

function BasicTechnology() {
  return (
    <Row style={{ justifyContent: "center", paddingBottom: "50px" }}>
      <Col xs={4} md={2} className="tech-icons">
        <DiDjango />
      </Col>
      <Col xs={4} md={2} className="tech-icons">
        <DiPhp />
      </Col>
      <Col xs={4} md={2} className="tech-icons">
        <FaLaravel />
      </Col>
      <Col xs={4} md={2} className="tech-icons">
        <SiJavascript />
      </Col>
      <Col xs={4} md={2} className="tech-icons">
        <DiReact />
      </Col>
      <Col xs={4} md={2} className="tech-icons">
        <SiCloudera />
      </Col>
      <Col xs={4} md={2} className="tech-icons">
        <SiCsharp />
      </Col>
      <Col xs={4} md={2} className="tech-icons">
        <DiWordpress />
      </Col>
    </Row>
  );
}

export default BasicTechnology;