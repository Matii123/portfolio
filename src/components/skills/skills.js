import React, { Component } from 'react'
import { Container} from "react-bootstrap";
import GoodTechnology from "./good-technology";
import BasicTechnology from "./basic-technology";
import Toolstack from "./tools";

import '../../css/skills.css';

export default class Skills extends Component {
    render() {
        return (
            <div>
              <Container fluid className="mt-4">
                <h1 className="text-center text-white">
                  Umiejętności
                </h1>
                <div>
                  <div className="text-center p-2 heading">
                    Technologie, które znam
                  </div>
                  <GoodTechnology />
                </div>
                <div>
                  <div className="text-center p-2 heading">
                    Technologie, z którymi pracowałem
                  </div>
                  <BasicTechnology />
                </div>
                <div>
                  <div className="text-center p-2 heading">
                    Narzędzia, których używam
                  </div>
                  <Toolstack />
                </div>
              </Container>
            </div>
        )
    }
}