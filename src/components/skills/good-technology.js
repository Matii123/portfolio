import React from "react";
import { Col, Row } from "react-bootstrap";
import {
  DiHtml5,
  DiBootstrap,
  DiCss3,
  DiPython,
} from "react-icons/di";
import { 
  SiMysql,
} from "react-icons/si";

function GoodTechnology() {
  return (
    <Row style={{ justifyContent: "center", paddingBottom: "50px" }}>
      <Col xs={4} md={2} className="tech-icons">
        <DiHtml5 />
      </Col>
      <Col xs={4} md={2} className="tech-icons">
        <DiCss3 />
      </Col>
      <Col xs={4} md={2} className="tech-icons">
        <DiBootstrap />
      </Col>
      <Col xs={4} md={2} className="tech-icons">
        <DiPython />
      </Col>
      <Col xs={4} md={2} className="tech-icons">
        <SiMysql />
      </Col>
    </Row>
  );
}

export default GoodTechnology;