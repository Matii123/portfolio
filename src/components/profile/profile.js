import React, {Component} from 'react';

import { Container, Col, Row } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { information, socials } from '../../date';

import '../../css/profile.css';

import picture_profile from '../../picture/picture.jpg';

import {faBitbucket, faLinkedin } from '@fortawesome/free-brands-svg-icons'


export default class Profile extends Component {
    render() {
        return (
            <div>
                <Container fluid className="profile-row">
                    <Row>
                        <Col>
                        </Col>
                        <Col sm={4} className="column-greeting">
                            <p className="column-greeting-name">{information.welcome_message}</p>
                            <p className="about-me-text">{information.about_me}</p>
                        </Col>
                        <Col sm={4} className="column-profile">
                            <div className="profile">
                                <img className="profile-img" src={picture_profile} alt="profile_picture"></img>
                            </div>
                            <div className="profile-socials">
                                <a className='profile-linkedin' href={socials.linkedinURL}>
                                    <FontAwesomeIcon icon = { faLinkedin } />
                                </a>
                                <a className='profile-bitbucket' href={socials.bitbucketURL}>
                                    <FontAwesomeIcon icon = { faBitbucket } />
                                </a>
                            </div>
                        </Col>
                        <Col>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}