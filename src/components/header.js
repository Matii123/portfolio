import React from 'react'
import 'bootstrap/dist/css/bootstrap.css'
import { Nav, Navbar } from 'react-bootstrap';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";

import Contact from '../components/contact/contact';
import Projects from '../components/projects/projects';
import Skills from '../components/skills/skills';
import Experience from '../components/experience/experience';
import Profile from '../components/profile/profile';

const Header = function() {
    return (
        <Router>
            <div>
                <Navbar bg="dark" variant="dark" sticky="top" expand="sm" collapseOnSelect>
                <Navbar.Brand as={Link} to="/" className="ml-3">
                    Portfolio
                </Navbar.Brand>
                <Navbar.Toggle/>
                    <Navbar.Collapse className="p-2">
                        <Nav>
                            <Nav.Link as={Link} to="/skills">Umiejętności</Nav.Link>
                            <Nav.Link as={Link} to="/experience">Doświadczenie</Nav.Link>
                            <Nav.Link as={Link} to="/projects">Projekty</Nav.Link>
                            <Nav.Link as={Link} to="/contact">Kontakt</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
            </div>
            <div>
                <Switch>
                    <Route exact path="/">
                        <Profile />
                    </Route>
                    <Route exact path="/experience">
                        <Experience />
                    </Route>
                    <Route path="/skills">
                        <Skills />
                    </Route>
                    <Route path="/projects">
                        <Projects />
                    </Route>
                    <Route path="/contact">
                        <Contact />
                    </Route>
                </Switch>
            </div>
        </Router>
    )
}

export default Header