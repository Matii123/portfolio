import React, { Component } from "react";
import {
  VerticalTimeline,
  VerticalTimelineElement,
} from "react-vertical-timeline-component";
import "react-vertical-timeline-component/style.min.css";

import { 
  sad_okregowy,
  urzad_miasta,
  webtechnika,
  UWM,
  Inforcavado,
  Ekonomik,
} from '../../date';

import{
    FaHourglassStart,
    FaUniversity,
} from "react-icons/fa";

import{
    MdSchool,
    MdOutlineWork,
} from "react-icons/md";

import { Container, Col, Row } from 'react-bootstrap';

export default class Experience extends Component {
  render() {
    return (
      <div>
        <Container fluid className="mt-4 mb-5">
          <h1 className="text-center text-white">
            Doświadczenie
          </h1>
          <Row className="mt-5">
            <Col>
              <VerticalTimeline>
              <VerticalTimelineElement
                className="vertical-timeline-element--work"
                date="06.2022-obecnie"
                dateClassName="text-white"
                iconStyle={{ background: 'darkgreen', color: 'white' }}
                icon={<MdOutlineWork />}
              >
                <h3 className="vertical-timeline-element-title">{sad_okregowy.work_station}</h3>
                <h4 className="vertical-timeline-element-subtitle">{sad_okregowy.company}</h4>
                <p>
                {sad_okregowy.description}
                </p>
              </VerticalTimelineElement>
              <VerticalTimelineElement
                className="vertical-timeline-element--work"
                date="09.2021"
                dateClassName="text-white"
                iconStyle={{ background: 'darkgreen', color: 'white' }}
                icon={<MdOutlineWork />}
              >
                <h3 className="vertical-timeline-element-title">{urzad_miasta.work_station}</h3>
                <h4 className="vertical-timeline-element-subtitle">{urzad_miasta.company}</h4>
                <p>
                {urzad_miasta.description}
                </p>
              </VerticalTimelineElement>
              <VerticalTimelineElement
                className="vertical-timeline-element--work"
                date="07.2021-08.2021"
                dateClassName="text-white"
                iconStyle={{ background: 'darkgreen', color: 'white' }}
                icon={<MdOutlineWork />}
              >
                <h3 className="vertical-timeline-element-title">{webtechnika.work_station}</h3>
                <h4 className="vertical-timeline-element-subtitle">{webtechnika.company}</h4>
                <p>
                {webtechnika.description}
                </p>
              </VerticalTimelineElement>
              <VerticalTimelineElement
                className="vertical-timeline-element--education"
                date="2018-2022"
                dateClassName="text-white"
                iconStyle={{ background: 'darkblue', color: 'white' }}
                icon={<FaUniversity />}
              >
                <h3 className="vertical-timeline-element-title">{UWM.work_station}</h3>
                <h4 className="vertical-timeline-element-subtitle">{UWM.company}</h4>
                <p>
                {UWM.description}
                </p>
              </VerticalTimelineElement>
              <VerticalTimelineElement
                className="vertical-timeline-element--education"
                date="2016"
                dateClassName="text-white"
                iconStyle={{ background: 'darkgreen', color: 'white' }}
                icon={<MdOutlineWork />}
              >
                <h3 className="vertical-timeline-element-title">{Inforcavado.work_station}</h3>
                <h4 className="vertical-timeline-element-subtitle">{Inforcavado.company}</h4>
                <p>
                {Inforcavado.description}
                </p>
              </VerticalTimelineElement>
              <VerticalTimelineElement
                className="vertical-timeline-element--education"
                date="2014-2018"
                dateClassName="text-white"
                iconStyle={{ background: 'darkblue', color: 'white' }}
                icon={<MdSchool />}
              >
                <h3 className="vertical-timeline-element-title">{Ekonomik.work_station}</h3>
                <h4 className="vertical-timeline-element-subtitle">{Ekonomik.company}</h4>
                <p>
                {Ekonomik.description}
                </p>
              </VerticalTimelineElement>
              <VerticalTimelineElement
                iconStyle={{ background: 'darkgreen', color: 'white' }}
                icon={<FaHourglassStart />}
              />
            </VerticalTimeline>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}