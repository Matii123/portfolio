import React from 'react'
import 'bootstrap/dist/css/bootstrap.css'
import { Container, Row, Col} from 'react-bootstrap';

const Footer = function() {
    return (
        <Container>
            <Row>
                <Col>
                <div className="footer text-center text-white p-1">
                    Portfolio Mateusz Białek &copy;{new Date().getFullYear()}
                </div>
                </Col>     
            </Row>
        </Container>
    )
}

export default Footer