// Basic information
export let information = {
    welcome_message: "Witaj na moim portfolio!",
    about_me: `Mam na imię Mateusz. Z wykształcenia jestem inżynierem informatyki. 
    Ukończyłem studia na kierunku Informatyki o specjalizacji Inżynieria Systemów Informatycznych na Uniwersytecie Warmińsko-Mazurskim w Olsztynie. 
    Interesuje się sportem, muzyką oraz programowaniem. 
    Wolny czas poświęcam na poszerzenie wiedzy z zakresu tworzenia aplikacji webowych. 
    Zapraszam do zapoznania się z moim portfolio oraz współpracy, ponieważ jestem w trakcie poszukiwania pracy/stażu w zawodzie programisty.`,
    profile_photo: "../picture/picture.jpg",
};

// Socials link
export let socials = {
    bitbucketURL: "https://bitbucket.org/Matii123/", // Bitbucket
    linkedinURL: "https://www.linkedin.com/in/mateusz-bia%C5%82ek-828870233/", // LinkedIn
};

// Projects information
export let portal_studencki_project = {
    title: "Portal internetowy dla studentów UWM",
    description: `Aplikacja była wykonana na pracę inżynierską. 
    Backend wykonałem w Django, natomiat Frontend aplikacji wykonałem w Bootstrapie. 
    Głównymi funkcjonalnościami aplikacji są opinie oraz forum, które miały na celu pomoc w wymianie informacji przez studentów, ale również osób, które dopiero będą zaczynały naukę na uniwersytecie. 
    Inną funkcjonalnością w systemie jest możliwość dodawania ogłoszeń jak również dodawania do harmonogramu wydarzeń występujących na uczelni. 
    Do tych danych miał uprawnienia tylko administrator, który miał oddzielny panel obsługujący te funkcjonalności.
    Administrator miał również możliwość usuwania komentarzy oraz postów, które uznał za niestosowne na stronie portalu.`,
    url: "https://bitbucket.org/Matii123/portal_studencki/src/dev/",
};
export let portfolio_project = {
    title: "Moje portfolio",
    description: `Strona stworzona do nauki działania frameworka React wraz z wykorzystaniem biblioteki Bootstrap. 
    Portfolio ma za zadanie przedstawić umiejętności oraz zdobyte doświadczenie, ale również przedstaiwa wykonane projekty.
    Przy projektach zawarte są krótkie opisy, w których zawarte są używane technologie oraz funkcjonowanie aplikacji.  
    Strona umożliwia skontaktowanie się z osobą bez użycia poczty internetowej.`,
    url: "https://bitbucket.org/Matii123/portfolio/src/master/",
};
export let metody_inżynierii_wiedzy_project = {
    title: "Algorytmy uczące",
    description: `Algorytmy wykonane były na przedmiot metody inżynierii wiedzy z użyciem Windows Forms oraz języka C#. 
    Wśród nich jest między innymi algorytm knn, algorytm generyczny oraz aplikacja generująca wagi w sieci neuronowej.`,
    url: "https://bitbucket.org/Matii123/metody-inzynierii-wiedzy/src/master/",
};
export let tetris_project = {
    title: "Gra tetris",
    description: `Gra stworzona na przedmiot projekt zespołowy. 
    Tematem projektu było Flash is dead, welcome HTML5, co oznaczało zamianę przestarzałego flasha na technologie obecnie używane takie jak m.in HTML5, CSS oraz Javascript. 
    Wynikiem projektu była gra tetris, która składała się z obrazków ludzi. 
    Po zakończeniu gry uzyskany wynik zostawał zapisywany w bazie danych.`,
};
export let przychodnia_project = {
    title: "Aplikacja przychodnia kortowo",
    description: `Aplikacja stworzona na przedmiot fakultatywny, która została zaimplementowana za pomocą frameworka Laravel oraz biblioteki Bootstrap. 
    Głównym zadaniem aplikacji była pomoc w funkcjonowaniu przychodni poprzez m.in. rezerwowanie i ewentualne anulowanie wizyty przez pacjentów, wydanie recept i przypisanie zaleceń przez lekarzy oraz możliwość przejrzenia historii leczenia.
    Rezerwacja wizyty polegała na wybrania lekarza, w którym wyświetlały się wolne terminy na poszczególne dni. 
    Po wybraniu terminu recepcja miała możliwość albo zaakceptowania takiej wizyty albo anulowania. 
    Zarezerwowane wizyty były widoczne w panelu użytkownika i wyświetlały stosowny komunikat do statusu wizyty.`,
};
export let kino_project = {
    title: "Aplikacja do zarządzania kinem",
    description: `Aplikacja została stworzona, w celu poszerzenia wiedzy z dwóch frameworków(Laravela oraz Vue). 
    Głównym zadaniem aplikacji było usprawnienie funkcjonowania kina poprzez m.in. wyświetlanie listy dostępnych seansów wraz z możliwością rezerwowania i ewentualnego jej anulowania.
    Po wybraniu seansu przez użytkownika, następuje przekierowanie do wybrania miejsca na sali kinowej. Wyświetlane są miejsca zarówno dostępne jak i zajęte, których nie ma możliwości wybrania. 
    Kiedy wszystko już zostało poprawnie wybrane, następuje przekierowanie do podsumowania rezerwacji. Wszystkie rezerwacje znajdują sie w zakładce konto.
    Aplikacja zawiera również panel administratora, który pozwala na zarządzanie seansami`,
    url: "https://bitbucket.org/Matii123/kino/src/dev/",
};


export let sad_okregowy = {
    work_station: "Informatyk",
    company: "Sąd okręgowy w Olsztynie",
    description: `Przygotowanie stanowisk komputerowych oraz przyjmowanie zgłoszeń wraz z ich rozwiązaniem.`,
};

export let urzad_miasta = {
    work_station: "Praktyka studencka",
    company: "Urząd miasta w Barczewie",
    description: `Diagnostyka oraz naprawa urzadzeń w urzędzie miasta. Pomoc przy drobnych problemach użytkowników.`,
};

export let webtechnika = {
    work_station: "Staż studencki - programista",
    company: "Webtechnika sp. z.o.o w Olsztynie",
    description: `Tworzenie aplikacji webowej, opartej o framework Django, której zadaniem było generowanie umów.`,
};

export let UWM = {
    work_station: "Studia inżynierskie",
    company: "Uniwersytet Warmińsko-Mazurski w Olsztynie",
    description: `Informatyka, Inżynieria Systemów informatycznych`,
};

export let Inforcavado = {
    work_station: "Serwisant sprzętu komputerowego",
    company: "Inforcavado, Barcelos",
    description: `Praktyka w ramach projektu Erasmus +`,
};

export let Ekonomik = {
    work_station: "Technik informatyk",
    company: "Technikum nr. 7 w Olsztynie",
    description: `Kwalifikacje E.12, E.13 i E.14`,
};